#!/bin/bash
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# Do not use this script manually!
# For `make vendor` use only.
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

# TODO поддержка разных дистрибутивов
. scripts/vendoring/libraries/_environment.bash

echo "VENDORING!"
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

sudo apt update
sudo apt install -y                                              \
    `# CERN ROOT dependencies`                                   \
    python3                                                      \
    openssl libssl-dev                                           \
    libx11-dev libxpm-dev libxft-dev libxext-dev                 \
    build-essential g++ gcc binutils make cmake cmake-curses-gui \
    `#TODO установка зависимостей для ядра Jupyter`              \
    `# Baical dependencies`                                      \
    unzip                                                        \
    `# BOOST dependencies`                                       \
    `# CMocka dependencies`                                      \
    `# CUnit dependencies`                                       \
    libtool                                                      \
    `# GLFW dependencies`                                        \
    libxinerama-dev libxcursor-dev libxi-dev                     \
    `# GLM dependencies`                                         \
    `# jemalloc dependencies`                                    \
    `# MPI dependencies`                                         \
    `# P7 dependencies`                                          \
    `# SDL2 dependencies`                                        \

if ! [ -d "vendor" ]; then
    echo "Directory 'vendor' does not exist => creaing directory"
    mkdir vendor
fi

cd vendor

bash ../scripts/vendoring/libraries/install.cuda.bash
bash ../scripts/vendoring/libraries/install.opengl.bash
bash ../scripts/vendoring/libraries/install.vulkan.bash
bash ../scripts/vendoring/libraries/build.all.bash "`pwd`/../scripts/vendoring/libraries/"

# cd ..
# TODO переместить cmake-файлы зависимостей (если они предоставляются) theconfigurator/cmake
# TODO test all
sudo apt autoremove
