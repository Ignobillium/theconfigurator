#!/bin/bash
sudo apt update
sudo apt-get install -y                                        \
    cmake pkg-config                                           \
    libao-dev libmpg123-dev                                    \
    libglew-dev libglfw3-dev libglm-dev                        \
    mesa-utils libglu1-mesa-dev freeglut3-dev mesa-common-dev  \

echo "GLFW will be builded by build.all.bash -> build.glfw.bash"
