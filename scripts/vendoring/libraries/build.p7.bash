#!/bin/bash
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # build Debug-configured P7
#

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# importing environment
export path="$1"
path="${path//\ /\\\ }"

. ${path}/_environment.bash

# Collecting & unpacking sources
collectSources ${P7}
tar -xvzf ${config[${P7}_SOURCES]}

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # Building Debug-configured P7
#

# mkdir build.p7.debug
# cd build.p7.debug

# # TODO More hyperparameters

# cmake -DCMAKE_INSTALL_PREFIX="../debug" \
#       -DCMAKE_BUILD_TYPE=Debug \
#       -DBUILD_EXAMPLES=ON \
#       -DBUILD_SHARED_LIBS=ON \
#       -DBUILD_TESTS=ON \
#       ../${config[${P7}_UNZIP]}

# make -j${CORES} install

# # # Cleaning up
# #
# mv -if ../debug/lib/libp7-baical/ ../debug/cmake/
# cd ..
# rm -rf build.p7.debug

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # build Release-configured P7
#
mkdir build.p7.release
cd build.p7.release

cmake -DCMAKE_INSTALL_PREFIX="../release" \
      -DCMAKE_BUILD_TYPE=Release \
      -DBUILD_EXAMPLES=ON \
      -DBUILD_SHARED_LIBS=ON \
      -DBUILD_TESTS=ON \
      ../${config[${P7}_UNZIP]}

make -j${CORES} install

# # Cleaning up
#
# mv -if ../release/lib/libp7-baical/ ../release/cmake/
cd ..
rm -rf build.p7.release

rm -rf ${config[${P7}_SOURCES]} ${config[${P7}_UNZIP]}
