#!/bin/bash
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# importing environment
export path="$1"
path="${path//\ /\\\ }"

. ${path}/_environment.bash

# Collecting & unpacking sources
collectSources ${CUNIT}
tar -xjf ${config[${CUNIT}_SOURCES]}

# Configuring
cd ${config[${CUNIT}_UNZIP]}
./bootstrap

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # building Debug-configured Baical                                          #
#                                                                             #
# ./configure \
#     CFLAGS="-Og -g"\
#     --prefix="`pwd`/../debug"

# make -j${CORES} install

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # building Release-configured Baical                                        #
#                                                                             #
./configure \
    CFLAGS="-Ofast"\
    --prefix="`pwd`/../release"

make -j${CORES} install

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # Cleaning up                                                               #
#                                                                             #
cd ..
rm -rf ${config[${CUNIT}_SOURCES]} ${config[${CUNIT}_UNZIP]}
