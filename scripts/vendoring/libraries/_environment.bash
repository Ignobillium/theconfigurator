#!/bin/bash

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # TODO docsting
#

#
# #
#
cat <<EOF
script:  $0
workdir: `pwd`
EOF

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # Hypers среды
#
CORES=$(lscpu | grep 'CPU(s)' |awk '{print $2}' | head -n 1)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # Specifying toolchain
#
# TODO specify C compiler
# TODO specify C++ compiler
# TODO specify python version
# TODO specify CMake version

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # Specifying vendor server parameters
#

export VENDOR_SERVER_URL="http://80.249.148.86:8088" #TODO http -> https
export VENDOR_SERVER_LOCATION="theconfigurator"

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # Unifying dependencies
#

export CUNIT="CUnit"
export CMOCKA="CMocka"
export BOOST="Boost"
export ROOT="Root"
export P7="libP7"
export BAICAL="Baical"
export MPI="OpenMPI"
export GLM="GLM"
export SDL2="SDL2"
export JEMALLOC="Jemalloc"

declare -A config
config[${CUNIT}_SOURCES]="CUnit-2.1-3.tar.bz2"
config[${CMOCKA}_SOURCES]="cmocka-master.zip"
config[${BOOST}_SOURCES]="boost_1_81_0.tar.gz"
config[${ROOT}_SOURCES]="root_v6.26.10.source.tar.gz"
config[${P7}_SOURCES]="libp7-baical.tar.gz"
config[${BAICAL}_SOURCES]="Baical_v5.3.1.zip"
config[${MPI}_SOURCES]="openmpi-4.1.4.tar.gz"
config[${GLM}_SOURCES]="glm-master.zip"
config[${GLFW}_SOURCES]="glfw-master.zip"
config[${SDL2}_SOURCES]="SDL-release-2.26.1.tar.gz"
config[${JEMALLOC}_SOURCES]="jemalloc-master.zip"

config[${CUNIT}_UNZIP]="CUnit-2.1-3"
config[${CMOCKA}_UNZIP]="cmocka-master"
config[${BOOST}_UNZIP]="boost_1_81_0"
config[${ROOT}_UNZIP]="root_v6.26.10.source"
config[${P7}_UNZIP]="libp7-baical"
config[${BAICAL}_UNZIP]="Baical_v5.3.1"
config[${MPI}_UNZIP]="openmpi-4.1.4"
config[${GLM}_UNZIP]="glm-master"
config[${GLFW}_UNZIP]="glfw-master"
config[${SDL2}_UNZIP]="SDL-release-2.26.1"
config[${JEMALLOC}_UNZIP]="jemalloc-master"

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # Helper functions
#

function checkDict {
    # TODO docstring
    case $1 in
        ${CUNIT}) echo true;;
        ${CMOCKA}) echo true;;
        ${BOOST}) echo true;;
        ${ROOT}) echo true;;
        ${P7}) echo true;;
        ${BAICAL}) echo true;;
        ${MPI}) echo true;;
        ${GLM}) echo true;;
        ${GLFW}) echo true;;
        ${SDL2}) echo true;;
        ${JEMALLOC}) echo true;;
        *) echo false;;
    esac
}

function collectSources {
    # TODO docstring
    # Передаётся единственный параметр - имя библиотеки

    if [ `checkDict $1` != true ]; then
        echo Invalid parameter: $1
        return
    fi

    if ! [ -f "${config[${1}_SOURCES]}" ]; then
        echo $1 doesnt exist
        # echo ${VENDOR_SERVER_URL}/${VENDOR_SERVER_LOCATION}/${config[${1}_SOURCES]}
        wget ${VENDOR_SERVER_URL}/${VENDOR_SERVER_LOCATION}/${config[${1}_SOURCES]}
    else
        echo "$1 exists, do nothing"
    fi
}
