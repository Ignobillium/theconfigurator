#!/bin/bash
# USAGE: check target
# if target==all, проверяет корректную установку всех пакетов
# else проверяет установку пакета target

function check() {
    if [[ "$1" == "all" ]]; then
        check_all
        return
    fi

    if [[ "$1" == "local" ]]; then
        check_local $2
        return
    fi

    echo "Checking target $2"
}

function check_all() {
    echo "Checking \`all\` mock"
    # echo ${TRUE}
}

function check_local() {
    echo "Checking \`local\` mock; target: $1"
    res=`dpkg -s $1`
    # echo ${TRUE}
}
