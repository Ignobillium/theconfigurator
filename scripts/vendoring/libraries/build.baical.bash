#!/bin/bash
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # TODO docs                                                                 #
# Первым параметром передаётся файл с конфигурацией окружения                 #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

# Importing environment
export path="$1"
path="${path//\ /\\\ }"

. ${path}/_environment.bash

# Collecting & unpacking sources
collectSources ${BAICAL}
unzip ${config[${BAICAL}_SOURCES]} -d ${config[${BAICAL}_UNZIP]}


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # building Debug-configured Baical                                          #
#                                                                             #
# mkdir build.baical.debug
# cd build.baical.debug

# cmake -DCMAKE_INSTALL_PREFIX="../debug" \
#       -DCMAKE_BUILD_TYPE=Debug \
#       -DBAICAL_EXTERNAL_INTEGRATION=TRUE \
#       ../${config[${BAICAL}_UNZIP]}

# make -j${CORES} install

# # Cleaning up
# cd ..
# rm -rf build.baical.debug

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # building Release-configured P7                                            #
#                                                                             #
mkdir build.baical.release
cd build.baical.release

cmake -DCMAKE_INSTALL_PREFIX="../release" \
      -DCMAKE_BUILD_TYPE=Release \
      -DBAICAL_EXTERNAL_INTEGRATION=TRUE \
      ../${config[${BAICAL}_UNZIP]}

make -j${CORES} install

# Cleaning up
cd ..
rm -rf build.baical.release ${config[${BAICAL}_UNZIP]} ${config[${BAICAL}_SOURCES]}
