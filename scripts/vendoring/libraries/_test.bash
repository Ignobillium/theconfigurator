#!/bin/bash
# USAGE: test target
# Проверяет корректную установку и работоспособность пакета target
# В качестве target может выступать all

function test() {
    if [[ "$1" == "all" ]]; then
        test_all
        return
    fi

    echo "Testing target \`$1\` mock"
}

function test_all() {
    echo "Testing \`all\` mock"
}
