#!/bin/bash
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# importing environment
export path="$1"
path="${path//\ /\\\ }"

. ${path}/_environment.bash

# Collecting & unpcaking sources
collectSources ${GLFW}
unzip ${config[${GLFW}_SOURCES]} #-d ${config[${GLFW}_UNZIP]}

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # building Debug-configured GLFW                                            #
#                                                                             #
# TODO CMAKE POSTFIX "_d" instead of "./debug" directory
# mkdir build.GLFW.debug
# cd build.GLFW.debug

# cmake -DCMAKE_INSTALL_PREFIX="../debug" \
#       -DCMAKE_BUILD_TYPE=Debug \
#       ../${config[${GLFW}_UNZIP]}

# make -j${CORES} install

# TODO test / check

# Cleaning up
#mv -ff ../debug/lib/cmake/GLFW/ ../debug/cmake
# rm -rf ../debug/lib/cmake

# cd ..
# rm -rf build.GLFW.debug

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # building Release-configured Baical                                        #
#                                                                             #

mkdir build.GLFW.release
cd build.GLFW.release

cmake -DCMAKE_INSTALL_PREFIX="../release" \
      -DCMAKE_BUILD_TYPE=Release \
      ../${config[${GLFW}_UNZIP]}

make -j${CORES} install

# TODO test / check

# Cleaning up
#mv -ff ../release/lib/cmake/GLFW/ ../release/cmake
rm -rf ../release/lib/cmake

cd ..
rm -rf build.GLFW.release

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # Cleaning up                                                               #
#                                                                             #

rm -rf ${config[${GLFW}_SOURCES]} -d ${config[${GLFW}_UNZIP]}
