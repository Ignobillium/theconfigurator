#!/bin/bash
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# importing environment
export path="$1"
path="${path//\ /\\\ }"

. ${path}/_environment.bash

# Collecting & unpacking sources
collectSources ${MPI}
tar -xvzf ${config[${MPI}_SOURCES]}

cd "${config[${MPI}_UNZIP]}"

# # # # # # # # # # # # # # # # # # # # #

# ./configure \
#     --prefix="`pwd`/../debug/" \
#     CFLAGS="-Og -g" \
#     CXXFLAGS="-Og -g"

# make install -j${CORES}

# # # # # # # # # # # # # # # # # # # # #

./configure \
    --prefix="`pwd`/../release/" \
    CFLAGS="-Ofast" \
    CXXFLAGS="-Ofast" # TODO -march native

make install -j${CORES}

# # # # # # # # # # # # # # # # # # # # #
rm -rf "${config[${MPI}_SOURCES]}" "${config[${MPI}_UNZIP]}"
