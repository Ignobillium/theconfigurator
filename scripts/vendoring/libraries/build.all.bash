#!/bin/bash
#Первым параметром передаётся путь до директории scripts/vendoring/libraries
export path="$1"
path="${path//\ /\\\ }"

echo "first param = ${path}"
echo "* * * * * * * * * * * * * * * * * * * * * * * * * * * *"
echo "* Building target baical                              *"
echo "*"                                                    #
bash "${path}/build.baical.bash" "${path}"                                #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
echo "* * * * * * * * * * * * * * * * * * * * * * * * * * * *"
echo "* Building target boost                               *"
echo "*"                                                    #
bash "${path}/build.boost.bash" "${path}"                                 #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
echo "* * * * * * * * * * * * * * * * * * * * * * * * * * * *"
echo "* Building target cmocka                              *"
echo "*"                                                    #
bash "${path}/build.cmocka.bash" "${path}"                                #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
echo "* * * * * * * * * * * * * * * * * * * * * * * * * * * *"
echo "* Building target cunit                               *"
echo "*"                                                    #
bash ${path}/build.cunit.bash ${path}                                 #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
echo "* * * * * * * * * * * * * * * * * * * * * * * * * * * *"
echo "* Building target glm                                 *"
echo "*"                                                    #
bash "${path}/build.glm.bash" "${path}"                                   #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
echo "* * * * * * * * * * * * * * * * * * * * * * * * * * * *"
echo "* Building target glfw                                *"
echo "*"                                                    #
bash "${path}/build.glfw.bash" "${path}"                                  #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
echo "* * * * * * * * * * * * * * * * * * * * * * * * * * * *"
echo "* Building target jemalloc                            *"
echo "*"                                                    #
bash "${path}/build.jemalloc.bash" "${path}"                              #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
echo "* * * * * * * * * * * * * * * * * * * * * * * * * * * *"
echo "* Building target mpi                                 *"
echo "*"                                                    #
#bash "${path}/build.mpi.bash" "${path}"                                   #
#В целом, mpi можно устанавливать с помощью менеджера пакетов
#Сборка вот так напрямую -- скорее запасной путь.
#Аналогично с boost
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
echo "* * * * * * * * * * * * * * * * * * * * * * * * * * * *"
echo "* Building target p7                                  *"
echo "*"                                                    #
bash "${path}/build.p7.bash" "${path}"                                    #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
echo "* * * * * * * * * * * * * * * * * * * * * * * * * * * *"
echo "* Building target root                                *"
echo "*"                                                    #
# bash "${path}/build.root.bash" "${path}"                                  #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
echo "* * * * * * * * * * * * * * * * * * * * * * * * * * * *"
echo "* Building target sdl2                                *"
echo "*"                                                    #
bash "${path}/build.sdl2.bash" "${path}"                                  #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
