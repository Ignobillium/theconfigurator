#!/bin/bash
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# importing environment
export path="$1"
path="${path//\ /\\\ }"

. ${path}/_environment.bash

# Collecting & unpcaking sources
collectSources ${GLM}
unzip ${config[${GLM}_SOURCES]} # -d ${config[${GLM}_UNZIP]}

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # building Debug-configured Baical                                          #
#                                                                             #
# mkdir build.GLM.debug
# cd build.GLM.debug

# cmake -DCMAKE_INSTALL_PREFIX="../debug" \
#       -DCMAKE_BUILD_TYPE=Debug \
#       ../${config[${GLM}_UNZIP]}

# make -j${CORES} install

# # TODO test

# # Cleaning up
# mv -if ../debug/lib/cmake/glm/ ../debug/cmake/
# rm -rf ../debug/lib/cmake

# cd ..
# rm -rf build.GLM.debug

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # building Release-configured Baical                                        #
#                                                                             #

mkdir build.GLM.release
cd build.GLM.release

cmake -DCMAKE_INSTALL_PREFIX="../release" \
      -DCMAKE_BUILD_TYPE=Release \
      ../${config[${GLM}_UNZIP]}

make -j${CORES} install

# TODO test

# Cleaning up
# mv -if ../release/lib/cmake/GLM/ ../release/cmake
# rm -rf ../release/lib/cmake

cd ..
rm -rf build.GLM.release

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # Cleaning up                                                               #
#                                                                             #

rm -rf ${config[${GLM}_SOURCES]} ${config[${GLM}_UNZIP]}
