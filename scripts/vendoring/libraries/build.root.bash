#!/bin/bash
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# importing environment
export path="$1"
path="${path//\ /\\\ }"

. ${path}/_environment.bash

# Collecting & unpacking sources
# collectSources ${ROOT}
# tar -xvzf ${config[${ROOT}_SOURCES]}
# TODO сборка ядра для jupyter
