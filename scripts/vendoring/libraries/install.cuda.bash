#!/bin/bash
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# CUDA 11-5 automatic installation (Ubuntu 22.04)
sudo apt -y install nvidia-cuda-toolkit

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# CUDA 11-7 manual installation (Ubuntu 20.04, Mint 20.03)
# TODO Добавить возможность указывать место куда скачивать прорву гигабайт

# sudo apt-get install -y apt-transport-https ca-certificates gnupg
# sudo apt-get install -y linux-headers-$(uname -r)

# wget https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/x86_64/cuda-ubuntu2004.pin

# sudo mv cuda-ubuntu2004.pin /etc/apt/preferences.d/cuda-repository-pin-600

# wget https://developer.download.nvidia.com/compute/cuda/11.7.1/local_installers/cuda-repo-ubuntu2004-11-7-local_11.7.1-515.65.01-1_amd64.deb

# sudo dpkg -i cuda-repo-ubuntu2004-11-7-local_11.7.1-515.65.01-1_amd64.deb

# sudo cp /var/cuda-repo-ubuntu2004-11-7-local/cuda-*-keyring.gpg /usr/share/keyrings/

# sudo apt-get update
# sudo apt-get -y install cuda

# rm cuda-repo-ubuntu2004-11-7-local_11.7.1-515.65.01-1_amd64.deb

# echo "export PATH=\$PATH:/usr/local/cuda-11/bin/" >> ~/.bashrc
# echo "export PATH=\$PATH:/usr/local/cuda-11/bin/" >> ~/.bash_profile

# echo "export LD_LIBRARY_PATH=\$LD_LIBRARY_PATH:/usr/local/cuda-11.7/lib64/" >> ~/.bashrc
# echo "export LD_LIBRARY_PATH=\$LD_LIBRARY_PATH:/usr/local/cuda-11.7/lib64/" >> ~/.bash_profile
