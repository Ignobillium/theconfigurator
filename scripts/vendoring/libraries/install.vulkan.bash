#!/bin/bash
# wget -qO- https://packages.lunarg.com/lunarg-signing-key-pub.asc | sudo tee /etc/apt/trusted.gpg.d/lunarg.asc
# sudo wget -qO /etc/apt/sources.list.d/lunarg-vulkan-jammy.list http://packages.lunarg.com/vulkan/lunarg-vulkan-jammy.list

sudo add-apt-repository ppa:graphics-drivers/ppa
sudo apt update
# sudo apt install nvidia-settings vulkan-utils vulkan-tools
sudo apt install -y                                        \
    libvulkan1 libvulkan-dev                               \
    vulkan-tools vulkan-validationlayers-dev               \
