#!/bin/bash
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# importing environment
export path="$1"
path="${path//\ /\\\ }"

. ${path}/_environment.bash

# Collecting & unpcaking sources
collectSources ${CMOCKA}
unzip ${config[${CMOCKA}_SOURCES]} # -d ${config[${CMOCKA}_UNZIP]}

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # building Debug-configured Baical                                          #
#                                                                             #
# mkdir build.cmocka.debug
# cd build.cmocka.debug

# cmake -DCMAKE_INSTALL_PREFIX="../debug" \
#       -DCMAKE_BUILD_TYPE=Debug \
#       ../${config[${CMOCKA}_UNZIP]}

# make -j${CORES} install

# # TODO test

# # Cleaning up
# mv -if ../debug/lib/cmake/cmocka/ ../debug/cmake
# rm -rf ../debug/lib/cmake

# cd ..
# rm -rf build.cmocka.debug

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # building Release-configured Baical                                        #
#                                                                             #

mkdir build.cmocka.release
cd build.cmocka.release

cmake -DCMAKE_INSTALL_PREFIX="../release" \
      -DCMAKE_BUILD_TYPE=Release \
      ../${config[${CMOCKA}_UNZIP]}

make -j${CORES} install

# TODO test

# Cleaning up
# mv -if ../release/lib/cmake/cmocka/ ../release/cmake
# rm -rf ../release/lib/cmake

cd ..
rm -rf build.cmocka.release

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # Cleaning up                                                               #
#                                                                             #

rm -rf ${config[${CMOCKA}_SOURCES]} ${config[${CMOCKA}_UNZIP]}
