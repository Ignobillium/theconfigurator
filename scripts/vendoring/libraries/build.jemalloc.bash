#!/bin/bash
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# importing environment
export path="$1"
path="${path//\ /\\\ }"

. ${path}/_environment.bash

# Collecting & unpcaking sources
collectSources ${JEMALLOC}
unzip ${config[${JEMALLOC}_SOURCES]} # -d ${config[${JEMALLOC}_UNZIP]}

# # # # # # # # # # # # # # # # # # # # # # # # # # #

cd ${config[${JEMALLOC}_UNZIP]}
sh autogen.sh

# # # # # # # # # # # # # # # # # # # # # # # # # # #
# Building Debug-configured Jemalloc
#
# ./configure \
#     --prefix="`pwd`/../debug/" \
#     --enable-debug \
#     --enable-prof \
#     --with-jemalloc-prefix="je" \
#     CFLAGS="-Og -g" \
#     CXXFLAGS="-Og -g"

# make -j${CORES} install

# # # # # # # # # # # # # # # # # # # # # # # # # # #
# Building Release-configured Jemalloc
#
./configure \
    --prefix="`pwd`/../release/" \
    --with-jemalloc-prefix="je" \
    CFLAGS="-Ofast" \
    CXXFLAGS="-Ofast"

make -j${CORES} install

cd ..
rm -rf "${config[${JEMALLOC}_SOURCES]}" "${config[${JEMALLOC}_UNZIP]}"
