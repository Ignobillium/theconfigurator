#!/bin/bash
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# importing environment
export path="$1"
path="${path//\ /\\\ }"

. ${path}/_environment.bash

# Collecting & unpacking sources
collectSources ${SDL2}
tar -xvzf ${config[${SDL2}_SOURCES]}

mkdir build.sdl2.release
cd build.sdl2.release

cmake -DCMAKE_INSTALL_PREFIX="../release" \
      -DCMAKE_BUILD_TYPE=Release \
      ../${config[${SDL2}_UNZIP]}

make -j${CORES} install

cd ..
rm -rf build.sdl2.release ${config[${SDL2}_SOURCES]} ${config[${SDL2}_UNZIP]}
