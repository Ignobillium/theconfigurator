#!/bin/bash
# if not ../.dependencies then mkdir ../.dependencies

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# importing environment
export path="$1"
path="${path//\ /\\\ }"

. ${path}/_environment.bash

# Collecting & unpacking sources
collectSources ${BOOST}
tar -xvzf ${config[${BOOST}_SOURCES]}

cd ${config[${BOOST}_UNZIP]}
sh bootstrap.sh

export BOOST_PREFIX="`pwd`/.."
export BOOST_PREFIX_DEBUG=${BOOST_PREFIX}/debug
export BOOST_PREFIX_RELEASE=${BOOST_PREFIX}/release

export BOOST_BUILDDIR=`pwd`/build.boost
export BOOST_STAGEDIR=`pwd`/boost.stage


# # build and install debug-configured BOOST
# ./b2 \
#      -j${CORES} \
#      --prefix=${BOOST_PREFIX_DEBUG} \
#      --exec-prefix=${BOOST_PREFIX_DEBUG}/bin \
#      --includedir=${BOOST_PREFIX_DEBUG}/include \
#      --libdir=${BOOST_PREFIX_DEBUG}/lib \
#      --cmakedir=${BOOST_PREFIX_DEBUG}/cmake \
#      --build-dir=${BOOST_BUILDDIR}/debug \
#      --stagedir=${BOOST_STAGEDIR}/debug \
#      \
#      variant=debug \
#      threading=multi \
#      \
#      install

# # build and install release-configured BOOST
# TODO указать опцию reconfigure all
./b2 \
     -j${CORES}                                                        \
     --prefix=${BOOST_PREFIX_RELEASE}                                  \
     --exec-prefix=${BOOST_PREFIX_RELEASE}/bin                         \
     --includedir=${BOOST_PREFIX_RELEASE}/include                      \
     --libdir=${BOOST_PREFIX_RELEASE}/lib                              \
     `#--cmakedir=${BOOST_PREFIX_RELEASE}/cmake`                       \
     --build-dir=${BOOST_BUILDDIR}/release                             \
     --stagedir=${BOOST_STAGEDIR}/release                              \
                                                                       \
     variant=release                                                   \
     threading=multi                                                   \
                                                                       \
     install

# # TODO clean this up
cd ..
rm -rf ${config[${BOOST}_SOURCES]} ${config[${BOOST}_UNZIP]}
